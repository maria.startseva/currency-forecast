from functools import lru_cache
from typing import Dict, Type

from api.core.base import AppEnvTypes, BaseAppSettings
from api.core.app import AppSettings
from api.core.dev import DevAppSettings
from api.core.prod import ProdAppSettings

environments: Dict[AppEnvTypes, Type[AppSettings]] = {
    AppEnvTypes.dev: DevAppSettings,
    AppEnvTypes.prod: ProdAppSettings,
    # AppEnvTypes.test: TestAppSettings,
}


@lru_cache
def get_app_settings() -> AppSettings:
    app_env = BaseAppSettings().app_env
    config = environments[app_env]
    return config()