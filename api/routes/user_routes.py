from fastapi import Form, APIRouter
# from fastapi.templating import Jinja2Templates
from pydantic import BaseModel
from typing import Optional

router = APIRouter()
# templates = Jinja2Templates(directory="api/templates")


class User(BaseModel):
    name: str
    token: str = None
    is_admin: bool = False
    role: Optional[str] = None


@router.post("/form_test/")
async def login(username: str = Form(default="user"), password: str = Form(default="0101")):
    return {"username": username}

