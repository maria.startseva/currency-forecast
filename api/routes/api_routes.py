from fastapi import Depends, Query, APIRouter
from fastapi.responses import HTMLResponse

from api.utils.output_tools import json2html
from api.utils.process import get_interactive_plot, predict

router = APIRouter()


class APIQueryParamsBase:
    def __init__(self,
                 base: str = Query('rub', example='rub', description="Base currency"),
                 currency: str = Query('usd', example="usd", description="Currency ['usd', 'eur']", max_length=3),
                 period: int = Query(180, example=180, description="Period for history"),
                 ):
        self.base = base
        self.currency = currency
        self.period = period


class APIQueryParamsForecast():
    def __init__(self,
                 base: str = Query('rub', example='rub', description="Base currency"),
                 currency: str = Query('usd', example="usd", description="Currency ['usd', 'eur']", max_length=3),
                 period: int = Query(180, example=180, description="Period to calculate predictions on its base"),
                 predictions: int = Query(42, example=42, description="Period for forecasting"),
                 output_fmt: str = Query('json', example="json", description="Output format ['json', 'html']"),
                 ):
        self.base = base
        self.currency = currency
        self.period = period
        self.predictions = predictions
        self.html = True if output_fmt != 'json' else False


@router.get("/")
async def root():
    body = """<html>
           <body style='padding: 10px;'>
           <h1>FastAPI</h1>
           </body>
           </html>"""

    return HTMLResponse(content=body)


# main API endpoint
@router.get('/forecast/{currency}')
async def get_forecast(params: APIQueryParamsForecast = Depends(), ):
    """
    This endpoint is used to get the forecast
    """

    result, _ = predict(currency=params.currency, period=params.period, predictions=params.predictions)
    result = json2html(json_file=result) if params.html else result

    return HTMLResponse(content=result)


@router.get('/history_plot/{currency}')
async def get_history(params: APIQueryParamsBase = Depends()):
    """
    This endpoint is used to get the interactive history plot
    """

    result_path = get_interactive_plot(currency=params.currency, period=params.period)
    with open(result_path, "r") as f:
        html = f.read()
    return HTMLResponse(content=html)


@router.get('/trend_plot/{currency}')
async def get_trend(params: APIQueryParamsBase = Depends(),
                    predictions=Query(42, example=42, description='Period for forecasting'),
                    plot_type=Query('trend', example='trend',
                                    description="Plot type: [trend, components]")):
    """
    This endpoint is used to get the interactive trend plot
    """
    predictions = int(predictions)
    result, (plot0, plot1) = predict(currency=params.currency, period=params.period, predictions=predictions,
                                     plots=True)
    plot = plot0 if plot_type == "components" else plot1

    with open(plot, "r") as f:
        html = f.read()

    return HTMLResponse(content=html)
