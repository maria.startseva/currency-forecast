from fastapi import File, UploadFile, APIRouter, Query
from fastapi.responses import FileResponse

import os

router = APIRouter()


@router.get("/file_download")
async def download_file_locally(file_path: str = Query(..., description="Path to the file to download")):
    return FileResponse(path=file_path, filename=file_path, media_type='json/application')


@router.post("/file_upload")
async def upload_file_locally(file: UploadFile = File(..., description="A file to read")):
    try:
        try:
            contents = file.file.read()
        except Exception as e:
            return {"error": f"[{type(e)}] {e}"}
        finally:
            file.file.close()

        file_path = f"./temp_data/{file.filename}"
        if os.path.exists(file_path):
            with open(file_path, "wb") as writer:
                writer.write(contents)
        else:
            return {"error": f"[PathError] Path doesn't exist"}

    except Exception as e:
        return {"error": f"[{type(e)}] {e}"}

    return {"filename": file.filename}