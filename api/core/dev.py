import logging

from api.core.app import AppSettings


class DevAppSettings(AppSettings):
    debug: bool = True

    title: str = "FastAPI endpoints for VSL API (dev)"

    logging_level: int = logging.DEBUG

    class Config(AppSettings.Config):
        env_file = ".env"