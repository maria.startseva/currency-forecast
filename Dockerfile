# docker build -f Dockerfile -t app:latest .

FROM ubuntu:20.04
RUN apt-get update && apt-get install --no-install-recommends -y python3.9 python3.9-dev python3-pip
RUN apt-get install ffmpeg libsm6 libxext6 --no-install-recommends -y
EXPOSE 8888
WORKDIR /app
COPY requirements.txt ./requirements.txt
RUN pip3 install -r requirements.txt
COPY . .
COPY run.sh ./run.sh
RUN chmod a+x run.sh
# For Ubuntu:
RUN apt-get install dos2unix
RUN dos2unix ./run.sh
CMD ["./run.sh"]