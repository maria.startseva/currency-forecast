import pandas as pd
import plotly.graph_objects as go
from plotly.subplots import make_subplots
from prophet import Prophet
from prophet.plot import plot_plotly, plot_components_plotly
from prophet.plot import add_changepoints_to_plot, plot

CURRENCIES = ["usd", "eur"]
PERIOD = 160


def _load_data(currency="usd"):
    usd_path = "./data/usd.xlsx"
    eur_path = "./data/eur.xlsx"
    file_path = usd_path if currency == "usd" else eur_path if currency == "eur" else None
    if file_path is None:
        raise NotImplementedError
    df = pd.read_excel(file_path)  # if __name__ != "__main__" else pd.read_excel("/../data/usd.xlsx")
    print(__name__)

    df['date'] = df.data.apply(lambda d: pd.to_datetime(d))
    df.set_index('date', inplace=True)

    return df


def _changepoints(model, forecast):
    fig = model.plot(forecast)
    plot_path = "changepoints.png"
    add_changepoints_to_plot(fig.gca(), model, forecast)
    fig.savefig(plot_path)


def _change_plotly_fig(fig, title=None, legend=True):
    fig.update_layout(
        title=f"{title}",
        font_family="Courier New",
        font_color="blue",
        title_font_family="Times New Roman",
        title_font_color="red",
        legend_title_font_color="green",
        showlegend=legend
    )

    # set showlegend property by name of trace
    for trace in fig['data']:
        if trace.name is None:
            trace['showlegend'] = False
    return fig


def predict(currency="usd", period=180, predictions=42, scale=0.1, plots=False):
    df_raw = _load_data(currency=currency)
    df = pd.DataFrame(data={'ds': df_raw.index, 'y': df_raw.curs.tolist()}, index=df_raw.index)
    predictions = 42 if predictions > 42 else 5 if predictions < 5 else predictions
    period = df.shape[0] if period > df.shape[0] else 60 if period < 60 else period
    df = df.iloc[-period:]
    df = df.iloc[:-predictions]

    model = Prophet(changepoint_prior_scale=scale, )
    model.fit(df)
    future = model.make_future_dataframe(periods=period)
    forecast = model.predict(future)
    res = forecast.loc[:, ['ds', 'yhat', 'yhat_lower', 'yhat_upper']].set_index('ds').T.to_json(indent=4)

    if plots:
        plot_path0 = "components.html"
        plot_path1 = "trend.html"
        fig0 = plot_components_plotly(model, forecast, figsize=(900, 300))
        fig1 = plot_plotly(model, forecast, changepoints=True, trend=True)
        fig1 = _change_plotly_fig(fig1, title=f"Forecast based on historical data for {period} days")
        fig0.write_html(plot_path0)
        fig1.write_html(plot_path1)

        return res, (plot_path0, plot_path1)

    return res, None


def _load_data_for_plot(currency="usd"):
    usd_path = "./data/USDRUB_220101_221119.csv"
    eur_path = "./data/EURRUB_220101_221119.csv"
    file_path = usd_path if currency == "usd" else eur_path if currency == "eur" else None
    if file_path is None:
        raise NotImplementedError

    df_raw = pd.read_csv(file_path)
    df_raw['date'] = df_raw["<DATE>"].apply(lambda d: pd.to_datetime(str(d), format='%Y%m%d'))
    df_raw['<DATE>'] = df_raw["<DATE>"].apply(lambda d: pd.to_datetime(str(d), format='%Y%m%d'))
    df_raw.set_index('date', inplace=True)
    return df_raw


def get_interactive_plot(currency="usd", period=180):
    plot_path = "test.html"
    df_raw = _load_data_for_plot(currency=currency)
    df = df_raw.iloc[-period:]
    fig = make_subplots(rows=2, cols=1, shared_xaxes=True,
                        vertical_spacing=0.03, subplot_titles=('OHLC', 'Volume'),
                        row_width=[0.2, 0.7])

    # Plot OHLC on 1st row
    fig.add_trace(go.Candlestick(x=df["<DATE>"], open=df["<OPEN>"], high=df["<HIGH>"],
                                 low=df["<LOW>"], close=df["<CLOSE>"], name="OHLC"),
                  row=1, col=1
                  )

    # Bar trace for volumes on 2nd row without legend
    fig.add_trace(go.Bar(x=df['<DATE>'], y=df['<VOL>'], showlegend=False), row=2, col=1)

    # Do not show OHLC's rangeslider plot
    fig.update(layout_xaxis_rangeslider_visible=False)
    fig.write_html(plot_path)
    return plot_path
