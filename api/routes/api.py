from fastapi import APIRouter

from api.routes import api_routes, file_routes, user_routes, healthcheck

router = APIRouter()
router.include_router(api_routes.router, tags=["api"], prefix="/api")
router.include_router(file_routes.router, tags=["files"], prefix="/files")
router.include_router(user_routes.router, tags=["users"], prefix="/users")
router.include_router(healthcheck.router, tags=["healthcheck"], prefix="/healthcheck")